import { Component } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrl: './calculator.component.css'
})
export class CalculatorComponent {
    hasil = 0

    value1 : number = 0
    value2 : number = 0

    plus(){
      return this.hasil = this.value1 + this.value2
    }

    minus(){
      return this.hasil = this.value1 - this.value2
    }

    multiplication(){
      return this.hasil = this.value1 * this.value2
    }

    division(){
      return this.hasil = this.value1 / this.value2
    }



}
