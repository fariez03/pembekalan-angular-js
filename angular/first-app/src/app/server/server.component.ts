import { Component } from "@angular/core";

@Component({
  selector: 'app-server',
  // template: `<p>server Component</p>`,
  templateUrl: './server.component.html',
  styleUrl: './server.component.css'
})

export class ServerComponent {
  serverId: number = Math.random()
  serverStatus: string = 'offline'
  // isTrue: boolean = true

  getServerStatus(){
    return this.serverStatus
  }

  constructor(){
    // ngStyle
    this.serverStatus = Math.random() >= 0.5 ? 'online' : 'offline'
  }

  getColor(){
    return this.serverStatus === 'online' ? 'lightgreen' : 'tomato'
  }



  // tugas -----------------------------------

    konten = ''
    status = false
    ikans = []
    nomor = 0

    getContent(){
      this.ikans.push(this.konten = Math.random() >= 0.5 ? 'tuna' : 'salmon')
      this.nomor += 1

      if(this.nomor % 2 == 0){
        this.status = false
      }else{
        this.status = true
      }

    }


}
