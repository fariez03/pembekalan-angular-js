import { Component } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrl: './servers.component.css'
})
export class ServersComponent {

    serverName = ''
    allowNewServer = false
    constructor(){
      setTimeout(() => {
        this.allowNewServer = true
      },2000)
    }

    // Event Binding

    serverCreationMessage = 'no server was created'
    serverCreated = false
    onCreateServer(){
      this.serverCreated = true
      this.serverCreationMessage = 'Server was created name is ' + this.serverName
    }

    onUpdateServerName(event: Event){
        console.log((<HTMLInputElement>event.target).value);
        this.serverName = (<HTMLInputElement>event.target).value
        // console.log((event.target||{}).value);
    }


    // Two Way Binding
    serverName2 = ''


    // Directive ngFor

    servers = ['TestServer','TestServer2']


    // tugas databinding -----------------
    userName = ''

    emptyField(){
      this.userName = ''
    }



}
