import { Component } from "@angular/core";

@Component({
  selector: 'app-danger-alert',
  template: `<p>danger-alert works!</p>`,
  styleUrl: './danger-alert.component.css'
})

export class DangerAlertComponent{}
