import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CategoryComponent } from './pages/category/category.component';
import { VariantComponent } from './pages/variant/variant.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CategoryService } from './services/category.service';
import { VariantService } from './services/variant.service';
import { ProductService } from './services/product.service';
import { ProductComponent } from './pages/product/product.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AppRoutingModule } from './app-routing.module';
import { OrderdetailComponent } from './pages/orderdetail/orderdetail.component';
import { OrderService } from './services/order.service';
import { OrderDetailService } from './services/order-detail.service';

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    VariantComponent,
    ProductComponent,
    NavbarComponent,
    SidebarComponent,
    OrderdetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    CategoryService,
    VariantService,
    ProductService,
    OrderService,
    OrderDetailService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
