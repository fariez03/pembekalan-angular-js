import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../config/baseurl';
import { OrderHeader } from '../models/orderheader';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http:  HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
        'Content-type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
    })
  }

  public getOrder(): Observable<OrderHeader[]>{
    return this.http.get<OrderHeader[]>(
      `${Config.url}/api/get-order-header`,
      this.httpOptions
    )
  }

  public addOrder(order: any): Observable<any> {
    return this.http.post<any>(`${Config.url}/api/add-orders`, order, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }
}
