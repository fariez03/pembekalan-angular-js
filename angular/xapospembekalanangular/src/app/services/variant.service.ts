import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Variant } from "../models/variant";
import { Config } from "../config/baseurl";

@Injectable({
  providedIn : 'root'
})

export class VariantService{
  constructor(private http:  HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
        'Content-type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
    })
  }

  public getVariant(): Observable<Variant[]>{
    return this.http.get<Variant[]>(
      `${Config.url}/api/get-variant`,
      this.httpOptions
    )
  }


  public getVariantByCategory(id): Observable<Variant[]>{
    return this.http.get<Variant[]>(
      `${Config.url}/api/get-variant-category/${id}`,
      this.httpOptions
    )
  }


  public addVariant(data: Variant): Observable<Variant> {
    return this.http.post<Variant>(`${Config.url}/api/add-variant`, data, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }

  public editVariant(data: Variant): Observable<Variant> {
    return this.http.put<Variant>(`${Config.url}/api/update-variant/${data.id}`, data, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }

  public deleteVariant(data: Variant): Observable<Variant> {
    return this.http.put<Variant>(`${Config.url}/api/delete-variant/${data.id}`, data, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }

}
