import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product';
import { HttpErrorResponse } from '@angular/common/http';
import { OrderDetail } from '../../models/orderdetail';
import { OrderService } from '../../services/order.service';
import { OrderHeader } from '../../models/orderheader';
import { OrderDetailService } from '../../services/order-detail.service';

@Component({
  selector: 'app-orderdetail',
  templateUrl: './orderdetail.component.html',
  styleUrl: './orderdetail.component.css'
})
export class OrderdetailComponent implements OnInit {
  public products: Product[] = []
  public orderHeader: OrderHeader[] = []
  public orderDetail: OrderDetail[] = []
  public listItem: OrderDetail[] = []
  public money = 0
  public reference = ''
  public searchProduct = ''
  public totalPages = []
  public page = 0
  public isShow = false;
  public totalData = 0

  constructor(
    private productService: ProductService,
    private orderService: OrderService,
    private orderDetailService: OrderDetailService
    ) { }

  ngOnInit() {
    this.getOrderHeaderAPI()
    document.getElementById('order-table').hidden = false;
    document.getElementById('order-detail-table').hidden = true;
    document.getElementById('transaction-table').hidden = true;
    document.getElementById('actionOrder').hidden = true;
  }

  public showNewTrans(){
    this.reference = '';
    this.listItem = [];
    this.money = 0

    // aksi toggle
    this.isShow = !this.isShow;


    // check kondisi toggle
    if (this.isShow) {
      document.getElementById('order-table').hidden = true;
      document.getElementById('order-detail-table').hidden = true;

      document.getElementById('transaction-table').hidden = false;
      document.getElementById('actionOrder').hidden = false;

      const aksi = document.querySelectorAll('.actionDisabled')
      aksi.forEach( arr => arr.removeAttribute('disabled'))
    }else{
      document.getElementById('order-table').hidden = false;
      document.getElementById('order-detail-table').hidden = true;

      document.getElementById('transaction-table').hidden = true;
      document.getElementById('actionOrder').hidden = true;
    }

  }

  public getOrderHeaderAPI() {
    this.orderService.getOrder().subscribe(
      (response: any) => {
        this.orderHeader = response.data
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public getOrderDetailAPI(id: number) {
    this.orderDetailService.getDetail(id).subscribe(
      (response: any) => {
        this.orderDetail = response.data
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public getProductAPI() {
    this.productService.getProduct().subscribe(
      (response: any) => {
        this.products = response.data
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public showDataOrder(data,mode){
    if(mode === 'detail'){
      this.getOrderDetailAPI(data)
      document.getElementById('order-table').hidden = true;
      document.getElementById('order-detail-table').hidden = false;
    }else{
      this.getOrderHeaderAPI()
      document.getElementById('order-table').hidden = false;
      document.getElementById('order-detail-table').hidden = true;
    }
  }

  public onOpenModal(mode: string) {
    const container = document.getElementById('main-container')
    const btn = document.createElement('button')
    const btnNewOrder = document.getElementById('search')

    btn.type = 'button'
    btn.style.display = 'none'
    btn.setAttribute('data-toggle', 'modal')

    if (mode === 'newOrder') {
      this.getProductAPI()
      btn.setAttribute('data-target', '#modal-new-order')
      btnNewOrder?.click()
    }

    if (mode === 'payment') {
      btn.setAttribute('data-target', '#modal-payment')
    }

    container!.appendChild(btn)
    btn.click()
  }

  public onSearchProduct(param) {
    this.productService.findProduct(param).subscribe(
      (response: any) => {
        this.products = response.data
        this.totalPages = new Array(response.total_page)
        this.totalData = response.total
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public actionPagination(data,mode){

    const btn = document.getElementById('search')

    if (mode === 'in') {
        this.page = data
    }else if(mode === 'next' && this.page != this.totalPages.length){
        this.page += 1
    }else if(mode === 'prev' && this.page != 0 ){
        this.page -=1
    }

    btn?.click()
  }

  public selectItemOrder(data) {
    const updateData: OrderDetail = {
      ...data,
      quantity: 1
    }
    this.listItem = [...this.listItem, updateData]
  }

  public calculateAmount(item) {
    return item.quantity * item.price
  }

  public sumQuantity(items) {
    let totalQty = 0
    for (let item of items) {
      totalQty += item.quantity
    }
    return totalQty
  }

  public totalAmount(items) {
    let total = 0
    for (let item of items) {
      total += (item.price * item.quantity)
    }
    return total
  }

  public orderPayment() {
    let grandTotal = this.totalAmount(this.listItem)
    const send = {
      listItem: this.listItem,
    }

    if (this.money < grandTotal) {
      alert('uang kurang')
    }else if(this.money < grandTotal){
        alert('kamu belum melakukan transaksi')
    } else {
      this.orderService.addOrder(send).subscribe(
        (response: any) => {
          const resObj = JSON.parse(response)
          this.reference = resObj.reference
        },
        (error: HttpErrorResponse) => {
          alert(error.message)
        }
      )

      const aksi = document.querySelectorAll('.actionDisabled')
      aksi.forEach( arr => arr.setAttribute('disabled','true'))
    }

  }

  public removeItemOrder(index) {
    this.listItem.splice(index, 1)
  }
}
