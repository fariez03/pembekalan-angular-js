export interface Category {
  id : number
  category_init : string
  category_name : string
  is_active : boolean
  create_by : string
  create_date : any
  modify_by : string
  modify_date : any
}
