export interface OrderHeader {
  id : number
  reference : string
  amount : number
  is_active : boolean
  create_by : string
  create_date : any
  modify_by : string
  modify_date : any
}
