export interface Variant {
  id : number
  category_id : string
  category_name : string
  variant_init : string
  variant_name : string
  is_active : boolean
  create_by : string
  create_date : any
  modify_by : string
  modify_date : any
}
