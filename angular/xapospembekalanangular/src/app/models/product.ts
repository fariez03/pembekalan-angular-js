export interface Product {
  id : number
  category_id : number
  category_name : string
  variant_id : number
  variant_name : string
  productinit : string
  productname : string
  description : string
  price : number
  stock : number
  isactive : boolean
  createby : string
  createdate : any
  modifyby : string
  modifydate : any
}
