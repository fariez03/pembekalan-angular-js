module.exports = exports = (app, pool) => {
    app.get("/api/getEmployee", (req,resp) => {
        const readQuery = `select * from employee where deleted_at is null`
        pool.query(readQuery, (error, result) => {            
            if (error) {
                return resp.send(400, {
                    success: false,
                    message: error
                })
            } else {
                return resp.send(200, {
                    success: true,
                    message: `Data loaded successfully!`,
                    data: result.rows
                })
            }
        })  
    })

    app.post("/api/createEmployee", (req,resp) => {
        const {nama_lengkap,tanggal_lahir,tempat_lahir} = req.body
        const insertQuery = `insert into employee (nama_lengkap,tanggal_lahir,tempat_lahir,create_by,create_date) 
                            values ('${nama_lengkap}','${tanggal_lahir}','${tempat_lahir}','admin1','now()')`

        pool.query(insertQuery, (error,result) => {
            if (error) {
                return resp.send(500, {
                    success: false,
                    message: error
                })
            }else{
                return resp.send(200, {
                    success: true,
                    message: `Data employee was created!`,
                })
            }
        })
    })

    app.put("/api/updateEmployee/:id", (req,resp) => {
        const id = req.params.id

        const checkId = `select count(*) from employee where id = ${id}`
        pool.query(checkId, (error,result) => {
            checkCountId = result.rows[0].count

            const {nama_lengkap,tanggal_lahir,tempat_lahir} = req.body
            if (checkCountId > 0) {
                const updateQuery = `update employee 
                                        set nama_lengkap = '${nama_lengkap}', tanggal_lahir = '${tanggal_lahir}',tempat_lahir = '${tempat_lahir}',
                                        modify_by = 'admin2',modify_date = 'now()' where id = ${id}`

                pool.query(updateQuery, (error,result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            message : error
                        })
                    }else{
                        return resp.send(200, {
                            success: true,
                            message: `Data ${nama_lengkap} was updated!`
                        })
                    }
                })
            }else{
                return resp.send(200, {
                    success: false,
                    message: `Data with id:${id} not found!`
                })
            }
        })

    })

    app.delete("/api/deleteEmployee/:id", (req,resp) => {
        const id = req.params.id

        const checkId = `select count(*) from employee where id = ${id}`
        pool.query(checkId, (error,result) => {
            const checkCountId = result.rows[0].count
            if (checkCountId > 0) {
                const deleteQuery = `update employee set deleted_at = 'now()' where id = ${id}`
                pool.query(deleteQuery, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            message : error
                        })
                    }else{
                        return resp.send(200, {
                            success: true,
                            message: `Data was deleted!`
                        })
                    }
                })
            }else{
                return resp.send(200, {
                    success: false,
                    message: `Data with id:${id} not found!`
                })
            }
        })
    })
}