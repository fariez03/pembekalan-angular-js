export interface OrderDetail {
    id : number
    header_id : number
    product_id : number
    quantity : number
    price : number
    is_active : boolean
    create_by : string
    create_date : any
    modify_by : string
    modify_date : any
    productname : string
}
