import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OrderDetail } from '../models/orderdetail';
import { Config } from '../config/baseurl';

@Injectable({
  providedIn: 'root'
})
export class OrderDetailService {

  constructor(private http:  HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
        'Content-type' : 'application/json',
        'Access-Control-Allow-Origin' : '*'
    })
  }

  public getDetail(id): Observable<OrderDetail[]> {
    return this.http.get<OrderDetail[]>(
      `${Config.url}/api/get-order-detail/${id}`,
      this.httpOptions
    )
  }

}
