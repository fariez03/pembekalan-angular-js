import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Product } from "../models/product";
import { Config } from "../config/baseurl";

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }

  public getProduct(): Observable<Product[]> {
    return this.http.get<Product[]>(
      `${Config.url}/api/get-product`,
      this.httpOptions
    )
  }

  public findProduct(param): Observable<Product[]> {
    return this.http.get<Product[]>(
      `${Config.url}/api/search-product?search=${param.value.searchName}&per_page=${param.value.searchPerPage}&page=${param.value.searchPage}`,
      this.httpOptions
    )
  }

  public addProduct(data: Product): Observable<Product> {
    return this.http.post<Product>(`${Config.url}/api/add-product`, data, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }

  public editProduct(data: Product): Observable<Product> {
    return this.http.put<Product>(`${Config.url}/api/update-product/${data.id}`, data, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }

  public deleteProduct(data: Product): Observable<Product> {
    return this.http.put<Product>(`${Config.url}/api/delete-product/${data.id}`, data, {
      ...this.httpOptions, responseType: 'text' as 'json'
    })
  }
}


