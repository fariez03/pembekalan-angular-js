import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Pembekalan Angular & ExpressJS';

  constructor(){

  }
  public onMenu(mode) {

    if (mode === 'category') {
      document.getElementById('category').hidden = false;
      document.getElementById('variant').hidden = true;
      document.getElementById('product').hidden = true;
    } else if(mode === 'variant') {
      document.getElementById('category').hidden = true;
      document.getElementById('variant').hidden = false;
      document.getElementById('product').hidden = true;
    }else {
      document.getElementById('category').hidden = true;
      document.getElementById('variant').hidden = true;
      document.getElementById('product').hidden = false;
    }

  }
}
