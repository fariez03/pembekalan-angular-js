import { Component, OnInit } from '@angular/core';
import { Variant } from '../../models/variant';
import { Category } from '../../models/category';
import { VariantService } from '../../services/variant.service';
import { CategoryService } from '../../services/category.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-variant',
  templateUrl: './variant.component.html',
  styleUrl: './variant.component.css'
})
export class VariantComponent implements OnInit {

  public variant: Variant[] = []
  public category: Category[] = []
  public dataVariant: Variant
  public dataCategory: Category
  // public deleteVariant: Variant

  constructor(private variantService: VariantService, private categoryService: CategoryService) {
    this.dataVariant = {} as Variant
    this.dataCategory = {} as Category
  }

  public getVariantAPI(): void {
    this.variantService.getVariant().subscribe((response: any) => {
      this.variant = response.data
    },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public getCategoryAPI(): void {
    this.categoryService.getCategory().subscribe((response: any) => {
      this.category = response.data
    },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  ngOnInit(): void {
    this.getVariantAPI()
    this.getCategoryAPI()
  }

  public onOpenModal(data: Variant, mode: String): void {

    const container = document.getElementById('main-container')
    const btn = document.createElement('button')


    btn.type = 'button'
    btn.style.display = 'none'
    btn.setAttribute('data-toggle', 'modal')

    if (mode === 'add') {
      btn.setAttribute('data-target', '#addVariantModal')
    }

    if (mode === 'edit') {
      this.dataVariant = data
      btn.setAttribute('data-target', '#editVariantModal')
    }

    container!.appendChild(btn)
    btn.click()
  }

  public onAddVariant(data: NgForm) {
    this.variantService.addVariant(data.value).subscribe(
        (response : any) => {
          this.getVariantAPI()
          const responseObject = JSON.parse(response)
          data.reset()
          document.getElementById('addCloseModal')?.click()
          alert(responseObject.message)
        },(error: HttpErrorResponse) => {
          const errorObject = JSON.parse(error.error)
          data.reset()
          alert(errorObject.message)
        }
    )
  }

  public onUpdateVariant(data: NgForm): void{
    this.variantService.editVariant(data.value).subscribe(
      (response: any) => {
        this.getVariantAPI()
          const responseObject = JSON.parse(response)
          data.reset()
          document.getElementById('editCloseModal')?.click()
          alert(responseObject.message)
        },(error: HttpErrorResponse) => {
          const errorObject = JSON.parse(error.error)
          data.reset()
          alert(errorObject.data)
      }
    )
  }

  public onDelete(data: Variant){

    const container = document.getElementById('main-container')
    const button = document.createElement('button')

    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-id', `${data.id}`)

    const resConfirm = confirm('Are you sure you want to delete this?')

    if (resConfirm) {

      this.variantService.deleteVariant(data).subscribe(
        (response: any) => {
          this.getVariantAPI()
            const responseObject = JSON.parse(response)
            alert(responseObject.message)
          },(error: HttpErrorResponse) => {
            const errorObject = JSON.parse(error.error)
            alert(errorObject.data)
        }

      )
    }


    container!.appendChild(button)
    button.click()

  }
}
