import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../models/category';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrl: './category.component.css'
})
export class CategoryComponent implements OnInit {        // implem... untuk menginisiasi ketika halaman dibuka

  public category: Category[] = []
  public editCategory: Category
  public deleteCategory: Category


  constructor(private categoryService: CategoryService){
    this.editCategory = {} as Category
    this.deleteCategory = {} as Category
  }

  ngOnInit(): void {
    this.getCategoryAPI()
  }

  public getCategoryAPI(): void{
      this.categoryService.getCategory().subscribe((response : any) => {
          this.category = response.data
        },
        (error: HttpErrorResponse) => {
          alert(error.message)
        }
      )
  }

  public onOpenModal(category: Category, mode: String): void {
    const container = document.getElementById('main-container')
    const button = document.createElement('button')

    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-toggle','modal')

    if(mode === 'add'){
      button.setAttribute('data-target','#addCategoryModal')
    }

    if(mode === 'edit'){
      this.editCategory = category
      button.setAttribute('data-target','#editCategoryModal')
    }

    if(mode === 'delete'){
      this.deleteCategory = category
      button.setAttribute('data-target','#deleteCategoryModal')
    }

    container!.appendChild(button)
    button.click()
  }


  public onAddCategory(addForm: NgForm): void{
    this.categoryService.addCategory(addForm.value).subscribe(
      (response: any) => {
        this.getCategoryAPI()
          const responseObject = JSON.parse(response)
          addForm.reset()
          document.getElementById('addCloseModal')?.click()
          alert(responseObject.message)
        },(error: HttpErrorResponse) => {
          const errorObject = JSON.parse(error.error)
          addForm.reset()
          alert(errorObject.message)
      }

    )
  }

  public onEditCategory(editForm: NgForm): void{

    this.categoryService.editCategory(editForm.value).subscribe(
      (response: any) => {
        this.getCategoryAPI()
          const responseObject = JSON.parse(response)
          editForm.reset()
          document.getElementById('editCloseModal')?.click()
          alert(responseObject.data)
        },(error: HttpErrorResponse) => {
          const errorObject = JSON.parse(error.error)
          editForm.reset()
          alert(errorObject.data)
      }
    )
  }


  public onDeleteCategory(deleteForm: NgForm): void{

    this.categoryService.deleteCategory(deleteForm.value).subscribe(
      (response: any) => {
        this.getCategoryAPI()
          const responseObject = JSON.parse(response)
          document.getElementById('deleteCloseModal')?.click()
          deleteForm.reset()
          alert(responseObject.data)
        },(error: HttpErrorResponse) => {
          const errorObject = JSON.parse(error.error)
          deleteForm.reset()
          alert(errorObject.data)
      }
    )
  }


}
