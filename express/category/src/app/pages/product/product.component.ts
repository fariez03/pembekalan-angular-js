import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product';
import { Variant } from '../../models/variant';
import { Category } from '../../models/category';
import { ProductService } from '../../services/product.service';
import { VariantService } from '../../services/variant.service';
import { CategoryService } from '../../services/category.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {
  public kategori = ''
  public products: Product[] = []
  public variants: Variant[] = []
  public categories: Category[] = []

  public dataProduct
  public dataVariant
  public dataCategory

  constructor(
    private productService: ProductService,
    private variantService: VariantService,
    private categoryService: CategoryService) {
    this.dataProduct = {} as Product
    this.dataVariant = {} as Variant
    this.dataCategory = {} as Category
  }

  public getProductAPI() {
    this.productService.getProduct().subscribe(
      (response: any) => {
      this.products = response.data
    },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public getVariantAPI() {
    this.variantService.getVariant().subscribe((response: any) => {
      this.variants = response.data
    },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public getVariantByCategory(event): void {
    const categoryId = (<HTMLSelectElement>event.target).value

    this.variantService.getVariantByCategory(categoryId).subscribe(
      (response: any) => {
        this.variants = response.data
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public getCategoryAPI(): void {
    this.categoryService.getCategory().subscribe((response: any) => {
      this.categories = response.data
    },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  ngOnInit() {
    this.getProductAPI()
    this.getCategoryAPI()
  }

  public onOpenModal(data, mode) {
    const container = document.getElementById('main-container')
    const btn = document.createElement('button')

    btn.type = 'button'
    btn.style.display = 'none'
    btn.setAttribute('data-toggle', 'modal')

    if (mode === 'add') {
      btn.setAttribute('data-target', '#addProductModal')
    }

    if (mode === 'edit') {
      this.getVariantAPI()
      this.dataProduct = data
      btn.setAttribute('data-target', '#editProductModal')
    }

    container!.appendChild(btn)
    btn.click()
  }

  public onAddProduct(data) {
    this.productService.addProduct(data.value).subscribe(
      (response: any) => {
        const responseObject = JSON.parse(response)
        data.reset()
        alert(responseObject.message)
        document.getElementById('addCloseModal')?.click()
      }, (error: HttpErrorResponse) => {
        const errorObject = JSON.parse(error.error)
        data.reset()
        alert(errorObject.message)
      }
    )
  }

  public onUpdateProduct(data) {
    this.productService.editProduct(data.value).subscribe(
      (response: any) => {
        this.getProductAPI()
        const responseObject = JSON.parse(response)
        data.reset()
        alert(responseObject.message)
        document.getElementById('editCloseModal')?.click()
      }, (error: HttpErrorResponse) => {
        const errorObject = JSON.parse(error.error)
        data.reset()
        alert(errorObject.message)
      }
    )

  }


  public onDelete(data: Product) {

    const container = document.getElementById('main-container')
    const button = document.createElement('button')

    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-id', `${data.id}`)

    const resConfirm = confirm('Are you sure you want to delete this?')

    if (resConfirm) {

      this.productService.deleteProduct(data).subscribe(
        (response: any) => {
          this.getProductAPI()
          const responseObject = JSON.parse(response)
          alert(responseObject.message)
        }, (error: HttpErrorResponse) => {
          const errorObject = JSON.parse(error.error)
          alert(errorObject.data)
        }

      )
    }

    container!.appendChild(button)
    button.click()
  }


}
