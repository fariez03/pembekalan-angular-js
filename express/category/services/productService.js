module.exports = exports = (app, pool) => {
    app.get("/api/get-product", (req, resp) => {
        const actQuery = `select  c.category_name,v.category_id ,v.variant_name , p.* from products p 
                            join variant v on p.variantId = v.id
                            join category c on v.category_id = c.id
                            order by id ;`

        pool.query(actQuery, (error, result) => {
            if (error) {
                return resp.send(500, {
                    success: false,
                    message: error.value
                })
            } else {
                return resp.send(200, {
                    success: true,
                    data: result.rows
                })
            }
        })
    })

    app.get('/api/search-product', (req, resp) => {

        let per_page =  req.query.per_page == '' ? 5 : req.query.per_page
        let page =  req.query.page == '' ? 1 : req.query.page
        const { search = ''} = req.query
        const actQuery = `select * from products where lower(productname) like lower('%${search}%') order by id offset (${page} - 1) * (${per_page}) limit ${per_page}`
        const queryCount = `select count(*) as count from products where lower(productname) like lower('%${search}%') `

        pool.query(queryCount, (error, result) => {
            if (error) {
                return resp.send(500, {
                    success: false,
                    message: error
                })
            } else {
                const total = result.rows[0].count
                const totalPage = Math.ceil(total / per_page)
                pool.query(actQuery, (error,result) => {
                    if (error) {
                        return resp.send(500, {
                            success : false,
                            message : error
                        })
                    }else{
                        return resp.send(200, {
                            success : true,
                            page : page,
                            per_page : per_page,
                            total : total,
                            total_page : totalPage,
                            data : result.rows
                        })
                    }
                })
            }
        })
    })

    app.post('/api/add-product', (req, resp) => {

        const { variantid, productinit, productname, description, price, stock, isactive } = req.body
        const duplicate = `select count(*) as count from products where productinit = '${productinit}' or productname = '${productname}'`
        const action = `insert into products (variantid,productinit,productname,description ,price,stock,isactive,createby,createdate)
                            values (${variantid},'${productinit}','${productname}','${description}', 
                                    ${price},${stock},${isactive},'admin1','now()')`

        if (variantid == null, productinit === '' || productname === '' || price === '' || stock === '' || isactive === '') {
            return resp.send(409, {
                success: false,
                message: `Cannot be empty`
            })
        }

        pool.query(duplicate, (error, result) => {
            if (error) {
                return resp.send(409, {
                    success: false,
                    message: error.value
                })
            }

            const countId = result.rows[0].count
            if (countId > 0) {
                return resp.send(500, {
                    success: false,
                    message: `Data already exists!`
                })
            } else {
                pool.query(action, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            message: error.value
                        })
                    } else {
                        return resp.send(200, {
                            success: true,
                            message: `Data ${productname} has been saved`
                        })
                    }
                })
            }
        })
    })

    app.put('/api/update-product/:id', (req, resp) => {
        const id = req.params.id
        const { variantid, productinit, productname, description, price, stock, isactive } = req.body
        const checkId = `select count(*) as count from products where id = ${id}`
        const duplicate = `select count(*) as count from products where (productinit = '${productinit}' or productname = '${productname}') and id = ${id}`
        const action = `update products 
                            set variantid = ${variantid}, productinit = '${productinit}', productname = '${productname}', 
                            description = '${description}', price = ${price}, stock = ${stock}, isactive = ${isactive}, modifyby = 'admin2', modifydate = 'now()'
                            where id = ${id}`

        if (variantid == null, productinit === '' || productname === '' || price === '' || stock === '' || isactive === '') {
            return resp.send(400, {
                success: false,
                message: `Cannot be empty`
            })
        }

        pool.query(checkId, (error, result) => {
            const countId = result.rows[0].count

            if (countId < 1) {
                return resp.send(404, {
                    success: false,
                    message: `Data not found`
                })
            } else {
                pool.query(duplicate, (error, result) => {
                    const countData = result.rows[0].count
                    if (countData > 0) {
                        return resp.send(409, {
                            success: false,
                            message: `Data already exists!`
                        })
                    } else {
                        pool.query(action, (error, result) => {
                            if (error) {
                                return resp.send(500, {
                                    success: false,
                                    message: `${error.value}`
                                })
                            } else {
                                return resp.send(200, {
                                    success: true,
                                    message: `Data ${productname} has been updated!`
                                })
                            }
                        })
                    }
                })
            }
        })
    })

    app.put('/api/delete-product/:id', (req, resp) => {
        const id = req.params.id
        const checkId = `select count(*) as count from products where id = ${id}`
        const action = `update products set isactive = false where id = ${id}`

        pool.query(checkId, (error, result) => {
            countId = result.rows[0].count
            if (countId < 1) {
                return resp.send(404, {
                    success: false,
                    message: `Data not found`
                })
            } else {
                pool.query(action, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            message: error.value
                        })
                    } else {
                        return resp.send(200, {
                            success: true,
                            message: `Data has been deleted!`
                        })
                    }
                })
            }
        })
    })
} 