module.exports = exports = (app, pool) => {       // app digunakan untuk mengambil express js nya & pool untuk memanggil koneksi databasenya
    app.get("/api/category", (req, resp) => {
        const query = `SELECT * FROM category`
        pool.query(query, (error, result) => {
            if (error) {
                return resp.send(400, {
                    success: false,
                    data: error
                })
            } else {
                return resp.send(200, {
                    success: true,
                    data: result.rows
                })
            }
        })
    })

    app.post("/api/addCategory", (req, resp) => {
        // var init = req.body.category_init
        // var name = req.body.category_name
        // var active = req.body.is_active
        const { category_init, category_name, is_active } = req.body

        if(category_init === '' || category_name === ''){
            return resp.send(409, {
                success: false,
                message: `Cannot be empty`
            })
        }

        const duplicateCheckQuery = `select count(*) as count from category 
                                    where category_init = '${category_init}' or category_name = '${category_name}'`
        pool.query(duplicateCheckQuery, (error, result) => {
            if (error) {
                return resp.send(500, {
                    success: false,
                    error: error
                })
            }

            const duplicateCount = result.rows[0].count
            if (duplicateCount > 0) {
                return resp.send(409, {
                    success: false,
                    data: `Data already exists`
                })
            } else {
                const query = `INSERT INTO category(category_init,category_name,is_active,create_by,create_date) 
        VALUES('${category_init}','${category_name}',${is_active}, 'admin1', 'now()')`

                pool.query(query, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            data: `An error accurred`
                        })
                    } else {
                        return resp.send(200, {
                            success: true,
                            message: `Data Category ${category_name} saved.`
                        })
                    }
                })
            }
        })


    })

    app.put("/api/updateCategory/:id", (req, resp) => {
        const id = req.params.id
        const { category_init, category_name, is_active } = req.body

        if(category_init === '' || category_name === ''){
            return resp.send(409, {
                success: false,
                message: `Cannot be empty`
            })
        }

        const checkId = `select count(*) from category where id = ${id}`
        pool.query(checkId, (error, result) => {
            checkIdCount = result.rows[0].count
            console.log(checkIdCount);
            if (checkIdCount < 1) {
                return resp.send(200, {
                    success: false,
                    data: `Data with id:${id} not found!`
                })
            } else {

                const duplicateCheckQuery = `select count(*) as count from category 
        where (category_init = '${category_init}' or category_name = '${category_name}') and id != ${id}`
                pool.query(duplicateCheckQuery, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            error: error
                        })
                    }

                    const duplicateCount = result.rows[0].count
                    if (duplicateCount > 0) {
                        return resp.send(409, {
                            success: false,
                            data: `Data already exists`
                        })
                    } else {
                        const query = `UPDATE category
                                        SET category_init = '${category_init}', category_name = '${category_name}', 
                                        is_active = ${is_active}, modify_by = 'admin2', modify_date = 'now()'
                                        WHERE id = ${id}`
                        pool.query(query, (error, result) => {
                            if (error) {
                                return resp.send(500, {
                                    success: false,
                                    error: error
                                })
                            } else {
                                return resp.send(200, {
                                    success: true,
                                    data: `Data Updated!`
                                })
                            }
                        })
                    }
                })

            }
        })

    })

    app.put("/api/deleteCategory/:id", (req,resp) => {
        const id = req.params.id
        const checkId = `select count(*) from category where id = ${id}`
        pool.query(checkId, (error, result) => {
            checkIdCount = result.rows[0].count
            console.log(checkIdCount);
            if (checkIdCount < 1) {
                return resp.send(409, {
                    success: false,
                    data: `Data with id:${id} not found!`
                })
            } else {
                const query = `UPDATE category SET is_active = false, modify_by = 'admin3', modify_date = 'now()'
                    WHERE id = ${id}`
                    pool.query(query, (error, result) => {
                        if (error) {
                            return resp.send(500, {
                                success: false,
                                error: error
                            })
                        } else {
                            return resp.send(200, {
                                success: true,
                                data: `Data was deleted!`
                            })
                        }
                    })
            }
        })
    })
}