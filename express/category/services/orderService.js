module.exports = exports = (app, pool) => {
    app.post('/api/add-orders', (req, resp) => {
        const { listItem } = req.body

        getReferenceTransaction((referenceCode) => {
            // console.log(referenceCode);

            let detailQuery = ''
            let amount = 0
            listItem.forEach((data, i) => {
                amount += data.price * data.quantity

                detailQuery += detailQuery.length > 0 ? ',' : ''
                detailQuery =
                    detailQuery + `((select id from inserted_orderheader), ${data.id}, ${data.quantity}, ${data.price}, true, 'admin1', 'now()')`
            })

            let advancedQuery = `with inserted_orderheader as (
                insert into orderheaders (reference,amount,is_active,create_by,create_date)
                values ('${referenceCode}',${amount},true,'admin1','now()')
                returning id
            ) insert into orderdetails (header_id,product_id,quantity,price,is_active,create_by,create_date)
            values ${detailQuery}`

            pool.query(advancedQuery, (error, result) => {
                if (error) {
                    return resp.send(400, {
                        success: false,
                        message: error
                    })
                } else {
                    return resp.send(200, {
                        success: true,
                        reference: referenceCode
                    })
                }
            })
        })

    })


    getReferenceTransaction = (callback) => {
        // SLS-24-03-0001
        // let tanggal = new Date('Mar 19 2024 12:33:34 GMT+0700')
        let tanggal = new Date()
        const year = String(tanggal.getFullYear()).substring(2)
        // const month = tanggal.getMonth() + 1 < 10 ? '0' + String(tanggal.getMonth() + 1)  :  tanggal.getMonth() + 1
        const month = ('0' + (tanggal.getMonth() + 1)).substring(-2)

        let newCode = 'SLS-' + year + month + '-'

        const checkQuery = `select max(reference) as reference from orderheaders`  // check backend
        pool.query(checkQuery, (error, result) => {
            if (error) {
                return {
                    success: false,
                    message: error
                }
            } else {
                if (result.rows.length === 0) {
                    newCode = newCode + '0001'
                    return callback(newCode)
                }
                let referenceData = result.rows[0].reference.split("-")
                let number = String(Number(referenceData[2]) + 1).padStart(4, "0")     // padStart : untuk string biar bisa seperti '0001' untuk number, biar nggak muncul "1" aja 
                newCode += number
                return callback(newCode)
            }
        })

    }

    app.get('/api/get-order-header', (req, resp) => {
        const actQuery = `select*from orderheaders order by id`

        pool.query(actQuery, (error,result) => {
            if (error) {
                return resp.send(500, {
                    success : false,
                    message : error.value
                })
            }else{
                return resp.send(200, {
                    success : true,
                    data : result.rows
                })
            }
        })
    })

    app.get('/api/get-order-detail/:id', (req,resp) => {
        let id = req.params.id
        const actQuery = `select  p.productname,o.*  from orderdetails o join products p ON o.product_id = p.id where o.header_id = ${id}`

        pool.query(actQuery, (error, result) => {
            if (error) {
                return resp.send(500, {
                    success : false,
                    message : error.value
                })
            }else{
                return resp.send(200, {
                    success : true,
                    data : result.rows
                })
            }
        })
    })

}