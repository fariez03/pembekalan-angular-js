module.exports = exports = (app, pool) => {
    app.get("/api/get-variant", (req, resp) => {
        
        const readQuery = `select c.category_name , v.* from category c join variant v ON c.id  = v.category_id order by id`
       
        pool.query(readQuery, (error, result) => {
            if (error) {
                return resp.send(400, {
                    success: false,
                    data: error
                })
            } else {
                return resp.send(200, {
                    success: true,
                    data: result.rows
                })
            }
        })
    })

    app.get('/api/get-variant-category/:id', (req,resp) => {
        const id = req.params.id
        const readQuery = `select id,variant_name from variant where category_id = ${id}`
       
        pool.query(readQuery, (error, result) => {
            if (error) {
                return resp.send(400, {
                    success: false,
                    error: error
                })
            } else {
                return resp.send(200, {
                    success: true,
                    data: result.rows
                })
            }
        })
    })

    app.post('/api/add-variant', (req, resp) => {

        const { category_id, variant_init, variant_name, is_active } = req.body
        const duplicateQuery = `select count(*) as count from variant v where variant_init = '${variant_init}' or variant_name = '${variant_name}'`
        const actionQuery = `insert into variant(category_id,variant_init,variant_name,is_active,create_by,create_date) 
                                values(${category_id},'${variant_init}','${variant_name}',${is_active},'admin1','now()')`

        if (category_id == null, variant_init === '' || variant_name === '') {
            return resp.send(409, {
                success: false,
                message: `Cannot be empty`
            })
        }


        pool.query(duplicateQuery, (error, result) => {
            if (error) {
                return resp.send(500, {
                    success: false,
                    error: error
                })
            }

            const countId = result.rows[0].count
            if (countId > 0) {
                return resp.send(409, {
                    success: false,
                    message: `Data already exists`
                })
            } else {
                pool.query(actionQuery, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            message: `${error}`
                        })
                    } else {
                        return resp.send(200, {
                            success: true,
                            message: `Data Variant ${variant_name} saved.`
                        })
                    }
                })
            }

        })

    })

    app.put('/api/update-variant/:id', (req, resp) => {
        const id = req.params.id
        const { category_id, variant_init, variant_name, is_active } = req.body
        const checkId = `select count(*) from variant where id = ${id}`
        const duplicateQuery = `select count(*) as count from variant v where (variant_init = '${variant_init}' or variant_name = '${variant_name}') and id != ${id}`
        const actionQuery = `update variant set category_id = ${category_id}, variant_init = '${variant_init}',
                                variant_name = '${variant_name}',is_active = ${is_active}, modify_by = 'admin2', modify_date = 'now()'
                                where id = ${id}`

        if (category_id == null || variant_init === '' || variant_name === '') {
            return resp.send(409, {
                success: false,
                message: `Cannot be empty`
            })
        }
        pool.query(checkId, (error, result) => {
            const countId = result.rows[0].count

            if (countId < 1) {
                return resp.send(404, {
                    success: false,
                    data: `Data with id:${id} not found!`
                })
            } else {
                pool.query(duplicateQuery, (error, result) => {
                    const duplicateCount = result.rows[0].count
                    if (duplicateCount > 0) {
                        return resp.send(409, {
                            success: false,
                            data: `Data already exists`
                        })
                    } else {
                        pool.query(actionQuery, (error, result) => {
                            if (error) {
                                return resp.send(500, {
                                    success: false,
                                    message: `${error.value}`
                                })
                            } else {
                                return resp.send(200, {
                                    success: true,
                                    message: `Data variant ${variant_name} was updated!`
                                })
                            }
                        })
                    }
                })
            }
        })

    })

    app.put('/api/delete-variant/:id', (req, resp) => {
        const id = req.params.id
        const checkId = `select count(*) from variant where id = ${id}`
        const actionQuery = `UPDATE variant SET is_active = false, modify_by = 'admin3', modify_date = 'now()' where id = ${id}`

        pool.query(checkId, (error, result) => {
            const countId = result.rows[0].count
            if (countId < 1) {
                return resp.send(409, {
                    success: false,
                    data: `Data with id:${id} not found!`
                })
            } else {
                pool.query(actionQuery, (error, result) => {
                    if (error) {
                        return resp.send(500, {
                            success: false,
                            error: error
                        })
                    } else {
                        return resp.send(200, {
                            success: true,
                            message: `Data was deleted!`
                        })
                    }
                })
            }
        })
    })
}